<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <!--[if lte IE 7]><?php print phptemplate_get_ie_styles(); ?><![endif]--><!--If Less Than or Equal (lte) to IE 7-->
  </head>
  <body<?php print phptemplate_body_class($left, $right); ?>>

    <!-- Layout -->
    <div id="wrapper">
      <?php if ($header_overlay): ?>
      <div id="header-overlay">
        <?php print $header_overlay; ?>
      </div>
      <?php endif; ?>
      <div id="header-wrapper">
        <div id="header">
          <?php if ($logo): ?>
          <div id="logo">
            <a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>">
              <img src="<?php print check_url($logo); ?>" alt="<?php print check_plain($site_name); ?>" />
            </a>
          </div>
          <?php endif; ?>
          <?php if ($site_slogan) : ?>
          <div id="siteslogan">
            <?php print check_plain($site_slogan); ?>
          </div>
          <?php endif; ?>
          <div id="feed-icons"><?php print $feed_icons; ?></div>
          <?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>
          <div id="header-content">
            <?php print $header; ?>
          </div>
        </div>
        <div id="nav">
          <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
          <?php endif; ?>
          <?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
          <?php endif; ?>
        </div> <!-- /#nav -->
      </div> <!-- /#header -->

      <div id="content-wrapper">

        <div id="center">
          <?php if ($left): ?>
          <div id="sidebar-left" class="sidebar">
            <?php print $left ?>
          </div> <!-- /#sidebar-left -->
          <?php endif; ?>
          <div id="content">
            <?php print $breadcrumb; ?>
            <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
            <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
              <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
              <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
            <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
            <?php if ($show_messages && $messages): print $messages; endif; ?>
            <?php print $help; ?>
            <?php print $content ?>
          </div>
          <?php if ($right): ?>
          <div id="sidebar-right" class="sidebar">
            <?php print $right ?>
          </div> <!-- /#sidebar-right -->
          <?php endif; ?>
        </div> <!-- /#center -->

      </div> <!-- /#container -->
      <div id="footer-wrapper">
        <div id="footer-container">
          <div id="footer">
            <?php print $footer_message; ?>
            <?php print $footer; ?>
          </div>
          <div id="footer-left">
            <?php print $footer_left; ?>
          </div>
          <div id="footer-middle">
            <?php print $footer_middle; ?>
          </div>
          <div id="footer-right">
            <?php print $footer_right; ?>
          </div>
        </div>
      </div> <!-- /#footer -->

    </div> <!-- /#wrapper -->
    <!-- /layout -->

    <?php print $closure ?>
    <?php print $scripts ?>
  </body>
</html>
